package com.example.serviceImpl;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang3.StringUtils;

import com.example.common.Constants;
import com.example.config.MessageResolver;
import com.example.models.UserBillDetails;
import com.example.models.UserTransactionDetails;
import com.example.repository.UserBillDetailsRepository;
import com.example.repository.UserTransactionDetailsRepository;
import com.example.request.UserBillDetailsRequest;
import com.example.service.FCMSenderPushNotificationService;
import com.example.service.UserBillDetailService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

@Service
@Transactional
public class UserBillDetailsServiceImpl implements UserBillDetailService{

	@Autowired
	private MessageResolver messageResolver;
	
	@Autowired
	private UserBillDetailsRepository billDetailsRepo;
	
	@Autowired
	private UserTransactionDetailsRepository transactionRepo;
	
	@Autowired
	private FCMSenderPushNotificationService sendPushNotification;
	
	@Autowired
	private Mapper mapper;
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"MM/dd/yyyy HH:mm:ss");
	
	@Override
	public String generateUserBill(UserBillDetailsRequest detailsRequest) throws NoSuchMessageException{
		String FCMtoken = Constants.FCMTOKEN;
		String content = StringUtils.EMPTY;

		// if noun is Bill and verb is pay then after its store into database
		if(detailsRequest.getNoun().equals(Constants.BILL) || detailsRequest.getVerb().equals(Constants.PAY) ) {
			UserBillDetails billDetails = mapper.map(detailsRequest, UserBillDetails.class);
			billDetailsRepo.save(billDetails);
			
			UserTransactionDetails transactionDetails = mapper.map(detailsRequest.getProperties(),UserTransactionDetails.class);
			transactionDetails.setUserId(billDetails.getUserId());
			transactionDetails.setBillId(billDetails);
			transactionRepo.save(transactionDetails);

			// send push notification for first pay by new user
			if(!billDetailsRepo.findByUserId(detailsRequest.getUserId()).isEmpty() && billDetailsRepo.findByUserId(detailsRequest.getUserId()).size() == 1) {
				sendPushNotification.sendPushNotification(FCMtoken, content, Constants.APPTYPE, new HashMap<String, Object>(), Constants.PAYACTION, true, Long.valueOf(billDetails.getUserId()));
			}
			//List of last 
			List<UserBillDetails> billDetail = billDetailsRepo.findLast5ByUserIdOrderByIdDesc(billDetails.getUserId());
			List<UserTransactionDetails> transactionDetail = transactionRepo.findLast5ByUserIdOrderByIdDesc(billDetails.getUserId());
			
			// to find out total number of time spent
			Integer totalTimeSpent = billDetail.stream().map(l -> l.getTimeSpent()).collect(Collectors.summingInt(Integer::intValue));
			// to find out total number amount pay in last 5 payments
			Integer totalamount = transactionDetail.stream().map(l -> Double.valueOf(l.getValue())).collect(Collectors.summingInt(Double::intValue));
			
			// this is for sent alert
			if(totalTimeSpent <= 300 && totalamount >= 20000) {
				System.out.println("You have done 5 Pay within 5 minutes time duration.");
			}
			// here we assume one feedBack table 
			sendFeedBackAlert();
			
			return "Bill Pay Successfully.";
		}else {
			return "Bill Not Pay Successfully.";
		}
	}
	
	/*
	 * Scheduler for 15 min
	 */
	@Scheduled(fixedDelay = 900000)
	public void sendFeedBackAlert() {
	
		System.out.println("You have to give feedBack if not given yet." + dateFormat.format(new Date()));

	
	}

}
