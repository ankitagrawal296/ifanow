package com.example.serviceImpl;

import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.common.Constants;
import com.example.config.MessageResolver;
import com.example.service.FCMSenderPushNotificationService;

@Service
public class FCMSenderPushNotificationServiceImpl implements FCMSenderPushNotificationService{
	
    private static final Logger LOGGER = Logger.getLogger(FCMSenderPushNotificationServiceImpl.class.getName());
	
	@Override
	public void sendPushNotification(String fcmToken, String content, String appType, Map<String, Object> metaData,
			String clickAction, boolean isClickable, Long userId) {
		
		Handler consoleHandler = null;
        Handler fileHandler  = null;
		String serverKey= Constants.SERVERKEY;
		try {
			// push notificatin data convert into string 
			consoleHandler = new ConsoleHandler();
            fileHandler  = new FileHandler("D:/Ifatest/Ifatest/logs/PushNotification.log"); 
            LOGGER.addHandler(consoleHandler);
            LOGGER.addHandler(fileHandler);
			metaData.put("Content-Type", "application/json");
			metaData.put("Authorization", "key="+serverKey);
			metaData.put("message",content);
			metaData.put("click_action",clickAction);
			metaData.put("isClickable",isClickable);
			metaData.put("to",fcmToken);
			metaData.put("priority",10);

			String notificationBody = metaData.toString();
			System.out.println("Notification Body : "+notificationBody);
			LOGGER.fine("Notification Body : "+notificationBody);
		
	
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
