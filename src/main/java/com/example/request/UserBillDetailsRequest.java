package com.example.request;

public class UserBillDetailsRequest {

	private Long id;
	
	private Integer userId;
	
	private String ts;
	
	private String noun;
	
	private String verb;
	
	private String latLong;
	
	private Integer timeSpent;

	private UserTransactionDetailsRequest properties;

	public UserTransactionDetailsRequest getProperties() {
		return properties;
	}

	public void setProperties(UserTransactionDetailsRequest properties) {
		this.properties = properties;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getNoun() {
		return noun;
	}

	public void setNoun(String noun) {
		this.noun = noun;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getLatLong() {
		return latLong;
	}

	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}

	public Integer getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}
	
}
