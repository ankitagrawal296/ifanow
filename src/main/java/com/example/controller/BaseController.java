package com.example.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.config.MessageResolver;
import com.example.response.BaseResponse;

public abstract class BaseController {

	@Autowired
	private MessageResolver messageResolver;
	
	protected ResponseEntity<BaseResponse> ok(Object data, String message) {
		return success(data, message, HttpStatus.OK);
	}

	protected ResponseEntity<BaseResponse> success(Object data, String message, HttpStatus httpStatus) {
		BaseResponse entity = new BaseResponse();

		entity.settStatus(true);
		entity.settHttpCode(httpStatus.value());
		entity.settData(data);
		entity.settMessage(message);
		
		return new ResponseEntity<BaseResponse>(entity, httpStatus);
	}
}
