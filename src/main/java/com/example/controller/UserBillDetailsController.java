package com.example.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.request.UserBillDetailsRequest;
import com.example.response.BaseResponse;
import com.example.service.UserBillDetailService;

@RestController
@RequestMapping(value = "api/v1/ifa")
public class UserBillDetailsController extends BaseController {	
	
	@Autowired
	private UserBillDetailService userBillDetailsService;
	
	/*
	 * @RequestBody UserBillDetailsRequest DTO
	 * return message
	 */
	@PostMapping(value = "/create")
	public ResponseEntity<BaseResponse> userBillPayEvent(@RequestBody UserBillDetailsRequest detailsRequest) {

		String messageKey = StringUtils.EMPTY;
		messageKey = userBillDetailsService.generateUserBill(detailsRequest);
		return ok(null, messageKey);
	}
	
	
}
