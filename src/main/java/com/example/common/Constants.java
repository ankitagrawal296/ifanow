package com.example.common;

public class Constants {

	public static final String FCMTOKEN = "fcm_token_random";
	
	public static final String APPTYPE = "user_app";
	
	public static final String PAYACTION = "pay_action";
	
	public static final String SERVERKEY = "server_key";
	
	public static final String BILL = "bill";
	
	public static final String PAY = "pay";
}
