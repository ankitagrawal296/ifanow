package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.models.UserBillDetails;
import com.example.models.UserTransactionDetails;

public interface UserTransactionDetailsRepository extends JpaRepository<UserTransactionDetails, Long> {

	List<UserTransactionDetails> findLast5ByUserIdOrderByIdDesc(Integer userid);
}
