package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.models.UserBillDetails;

import java.util.List;

public interface UserBillDetailsRepository extends JpaRepository<UserBillDetails, Long>{

	List<UserBillDetails> findByUserId(Integer userid);
	
	List<UserBillDetails> findLast5ByUserIdOrderByIdDesc(Integer userid);
	
}
