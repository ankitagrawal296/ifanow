package com.example.response;

public class BaseResponse {

	private Boolean tStatus;
	private String tMessage;
	private Integer tHttpCode;
	private Object tData;

	public Boolean gettStatus() {
		return tStatus;
	}

	public void settStatus(Boolean tStatus) {
		this.tStatus = tStatus;
	}

	public String gettMessage() {
		return tMessage;
	}

	public void settMessage(String tMessage) {
		this.tMessage = tMessage;
	}

	public Integer gettHttpCode() {
		return tHttpCode;
	}

	public void settHttpCode(Integer tHttpCode) {
		this.tHttpCode = tHttpCode;
	}

	public Object gettData() {
		return tData;
	}

	public void settData(Object tData) {
		this.tData = tData;
	}

}
