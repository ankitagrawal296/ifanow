package com.example.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;

@Entity
@Table(name = "ifa_bill_details")
public class UserBillDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "ts")
	private String ts;
	
	@Column(name = "noun")
	private String noun;
	
	@Column(name = "verb")
	private String verb;
	
	@Column(name = "lat_long")
	private String latLong;
	
	@Column(name = "timespent")
	private Integer timeSpent;

	@OneToOne(mappedBy="billId")
	private UserTransactionDetails transactionDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getNoun() {
		return noun;
	}

	public void setNoun(String noun) {
		this.noun = noun;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getLatLong() {
		return latLong;
	}

	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}

	public Integer getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

	public UserTransactionDetails getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(UserTransactionDetails transactionDetails) {
		this.transactionDetails = transactionDetails;
	}
	
}
