package com.example.service;

import java.util.Map;

public interface FCMSenderPushNotificationService {

	public void sendPushNotification(final String fcmToken, final String content, String appType, Map<String, Object> metaData,
			String clickAction,boolean isClickable, Long user);
}
