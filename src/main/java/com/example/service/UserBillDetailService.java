package com.example.service;

import com.example.request.UserBillDetailsRequest;

public interface UserBillDetailService {

	String generateUserBill(UserBillDetailsRequest detailsRequest);

}
